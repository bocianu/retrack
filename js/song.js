const RMTsong = () => {
	
	const songData = {}

	const emptySong = {
		channelCount : 4,
		song : [
			[0x00, 0x01, 0x02, 0x03]

		],
		patterns : [

		],
		instruments : [

		]
	}

    clearSongData = () => {
		_.assign(songData, emptySong)
	}

    clearSongData();


	return {
		data:songData,
		clearSongData,
	}
};
