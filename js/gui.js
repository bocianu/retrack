const gui = (song) => {
	$('<ul/>').attr('id','menulist').addClass('menulist').appendTo('#menu');

	const addMenuItem = (name, handler, parent = 'menulist') => {
		$('<li/>').html(name).addClass('menuitem').appendTo(`#${parent}`)
    .bind('click', handler);
	}

	const EDIT_PATTERN = 0;
	const EDIT_SONG = 1;
	const EDIT_INTRUMENT = 2;

	const noteBaseNames = ['C-', 'C#', 'D-', 'D#', 'E-', 'F-', 'F#', 'G-', 'G#', 'A-', 'A#', 'H-'];
	const noteNames = _.times(61,note => `${noteBaseNames[note % 12]}${Math.floor(note/12)+1}`)

	const keyMap1 = [
			[90], // 0
			[83], //1
			[88], //2
			[68], //3
			[67], //4
			[86], //5
			[71], //6
			[66], //7
			[72], //8
			[78], //9
			[74], //10
			[77], //11
			[188,81], //12
			[76,50], //13
			[190,87], //14
			[186,51], //15
			[191,69], //16
			[82], //17
			[53], //18
			[84], //19
			[54], //20
			[89], //21
			[55], //22
			[85], //23
			[73], //24
			[57], //25
			[79], //26
			[48], //27
			[80], //28
			[219], //29
			[187], //30
			[221] //31
	];
	
	const editor = {
		songPosition: 0,
		songRow: 0,
		window: 'song',
		cursor : {
			track:0,
			row:0,
			field:0,
			step:1
		},
		mode: EDIT_PATTERN,
		baseOctave : 1,
		defaultVolume : 0xF,
		currentInstrument : 0
	}


	createNoteKeyMap = kmap => {
		const map = [];
		_.each(kmap, (v,note) => {
			_.each(v, kcode => { map[kcode] = note })
		})
		map[32] = 62; // empty row
		return map;
	}

	const noteKeyMap = createNoteKeyMap(keyMap1);

	const hexKeyMap = [];
	hexKeyMap[48] = '0';
	hexKeyMap[49] = '1';
	hexKeyMap[50] = '2';
	hexKeyMap[51] = '3';
	hexKeyMap[52] = '4';
	hexKeyMap[53] = '5';
	hexKeyMap[54] = '6';
	hexKeyMap[55] = '7';
	hexKeyMap[56] = '8';
	hexKeyMap[57] = '9';
	hexKeyMap[65] = 'a';
	hexKeyMap[66] = 'b';
	hexKeyMap[67] = 'c';
	hexKeyMap[68] = 'd';
	hexKeyMap[69] = 'e';
	hexKeyMap[70] = 'f';

	const getCursorInput = () => {
		console.log(editor.window);
		switch (editor.window) {
			case 'patt':
				return $(`#${editor.window}_${editor.cursor.track}_${editor.cursor.row}`).children().eq(editor.cursor.field);
				
			case 'song':
				console.log($(`#${editor.window}_${editor.cursor.track}_${editor.songRow}`).children().eq(0));
				return $(`#${editor.window}_${editor.cursor.track}_${editor.songRow}`).children().eq(0);
				
		} 
	}		
		
	const getCursorCell = () => $(`#${editor.window}_${editor.cursor.track}_${editor.cursor.row}`);
	const getCursorNote = () => $(`#${editor.window}_${editor.cursor.track}_${editor.cursor.row}`).children().eq(0).val();
	const getCursorInstr = () => $(`#${editor.window}_${editor.cursor.track}_${editor.cursor.row}`).children().eq(1).val();
	const getCursorVol = () => $(`#${editor.window}_${editor.cursor.track}_${editor.cursor.row}`).children().eq(2).val();
	const isNoteUnderCursor = () => {
		//console.log(getCursorNote());
		return _.includes(noteNames, getCursorNote())

	}
	const toHex = (d, padding = 2, toUpper = true) => {
		let hex = Number(d).toString(16);
		while (hex.length < padding) {
			hex = "0" + hex;
		}
		if (toUpper) {
			hex = hex.toUpperCase();
		}
		return hex;
	}

	const setFocusOnCursor = () => {
		getCursorInput().focus();
	}

	const getCursorPosFromEvent = ev => {
		const cellId = $(ev.target).parent().attr('id');
		let row;
		[editor.window, editor.cursor.track, row] = _.split(cellId,'_').map((v,k)=>k>0?Number(v):v);
		if (editor.window == 'patt') {
			editor.cursor.row = row;
			editor.cursor.field = $(ev.target).index();
			editor.mode = EDIT_PATTERN;
		} 
		if (editor.window == 'song') {
			editor.songRow = row;
			editor.mode = EDIT_SONG;
		} 
	}

	const onCellFocus = ev => {
		getCursorPosFromEvent(ev);
	}

	const isArrowKey = keyCode => {
		return _.includes([37,38,39,40],keyCode);
	}

	const cursorMoveLeft = () => {
		if (editor.window == 'patt') {
			--editor.cursor.field;
			if (editor.cursor.field < 0) {
				editor.cursor.field = 2;
				--editor.cursor.track;
				if (editor.cursor.track < 0) {
					editor.cursor.track = 3;
				}
			}
		}
		if (editor.window == 'song') {
			--editor.cursor.track;
			if (editor.cursor.track < 0) {
				editor.cursor.track = 3;
			}
		}
		setFocusOnCursor();
	}

	const cursorMoveRight = () => {
		if (editor.window == 'patt') {
			++editor.cursor.field;
			if (editor.cursor.field > 2) {
				editor.cursor.field = 0;
				++editor.cursor.track;
				if (editor.cursor.track > 3) {
					editor.cursor.track = 0;
				}
			}
		}
		if (editor.window == 'song') {
			++editor.cursor.track;
			if (editor.cursor.track > 3) {
				editor.cursor.track = 0;
			}
		}
		setFocusOnCursor();
	}

	const cursorMoveUp = (step) => {
		if (editor.window == 'patt') {
			editor.cursor.row -= step;
			if (editor.cursor.row < 0) {
				editor.cursor.row += 32;
			}
		}
		if (editor.window == 'song') {
			editor.songRow -= 1;
			if (editor.songRow < 0) {
				editor.songRow += 255;
			}
		}
		setFocusOnCursor();
	}

	const cursorMoveDown = (step) => {
		if (editor.window == 'patt') {
			editor.cursor.row += step;
			if (editor.cursor.row > 31) {
				editor.cursor.row -= 32;
			}
		}
		if (editor.window == 'song') {
			editor.songRow += 1;
			if (editor.songRow > 254) {
				editor.songRow -= 255;
			}
		}
		setFocusOnCursor();
	}

	const cursorKeyMove = keyCode => {
		switch (keyCode) {
			case 37: // left
				cursorMoveLeft();
				break;
			case 39: // right
				cursorMoveRight();
				break;
  			case 38: // up
			  	cursorMoveUp(1);
				break;
			case 40: // down
				cursorMoveDown(1);
				break;
			default:
				break;
		}
	
	}


	const putNoteOnCursor = (note, instrument, volume) => {
		const cell = getCursorCell();
		const inputs = [ 
			(note<61)?noteNames[note]:'---', 
			(note<61)?toHex(instrument,2):'--', 
			(note<62)?toHex(volume,1):'-'
		];
		cell.children().each((idx,input)=>{$(input).val(inputs[idx])});
	}

	const putPatternOnCursor = (patt) => {
		const cell = getCursorInput();
		if (patt == 0xff) {
			cell.val('--');
		} else {
			cell.val(toHex(patt));
		}
		
	}


	const validate = {
		'note' : function(key) {
			const note = noteKeyMap[key];
			if (_.isFinite(note) && note < 61) {
				putNoteOnCursor(note, editor.currentInstrument, editor.defaultVolume);
				cursorMoveDown(editor.cursor.step);
				return true
			}
		},
		'vol' : function(key) {
			const hex = hexKeyMap[key];
			if (hex != undefined) {
				const volume = parseInt(hex, 16);
				if (isNoteUnderCursor()) {
					putNoteOnCursor(_.indexOf(noteNames , getCursorNote()), parseInt(getCursorInstr(),16), volume);
				} else {
					putNoteOnCursor(61, 0, volume);
				}
				cursorMoveDown(editor.cursor.step);
				return true
			}
		},
		'instr' : function(key) {
			if (isNoteUnderCursor()) {
				const hex = hexKeyMap[key];
				if (hex != undefined) {
					let instr = _.tail(getCursorInstr());
					if (instr > '3') {instr = '3'}
					let instrDec = parseInt(instr + hex,16);
					putNoteOnCursor(_.indexOf(noteNames , getCursorNote()), instrDec, parseInt(getCursorVol(),16));
					return true
				}
			}
		},
		'pattern' : function(key) {
			const hex = hexKeyMap[key];
			if (hex != undefined) {
				let instr = _.tail(getCursorInput().val());
				let instrDec = parseInt(instr + hex,16);
				putPatternOnCursor(instrDec);
				return true
			}
		}

	}



	const validateKey = (key, validator) => {
		if (validate[validator]) {
			return validate[validator](key);
		} else {
			throw (`Unknown validator: ${validator}`);
			return false;
		}
	}

	const UpdateInputOnCursor = (content) => {
		getCursorInput().val(content);
	}

	
	const readKey = ev => {
		const key = ev.keyCode || ev.which;
		console.log(`keyCode:${key}`)
		ev.preventDefault();
		ev.stopPropagation();
		if (isArrowKey(key)) {
			return cursorKeyMove(key);
		}
		if (key == 32) { // space
			if (editor.window == 'patt') {
				putNoteOnCursor(62, 0, 0);
				return cursorMoveDown(editor.cursor.step);
			}
			if (editor.window == 'song') {
				putPatternOnCursor(0xff);
				return cursorMoveDown(1);
			}
		}
		const validator = $(ev.target).attr('validator');
		if (validator) {
			return validateKey(key,validator);
			/*
			if (_.isString(content)) {
				UpdateCellOnCursor(content);
				return true;
			};
			return false;
			*/
		} 

	}

	const createTrackCell = (track, row) => {
		const note = $('<input/>',{
			'type':'text',
			'class':'trackNote trackInput',
			'value':'---',
			'validator': 'note'
		});
		const instr = $('<input/>',{
			'class':'trackInstr trackInput',
			'value':'--',
			'validator': 'instr'
		});
		const vol = $('<input/>',{
			'class':'trackVol trackInput',
			'value':'-',
			'validator': 'vol'
		});
				
		const cell = $('<div/>',{
			'id': `patt_${track}_${row}`,
			'class':'trackCell'
		}).append(note,instr,vol);
		cell.children('input').focus(onCellFocus).keydown(readKey);
		return cell;
	}

	const createTrack = (trackNum, track) => {
		const t = $('<div/>',{
			'id': 'track',
			'class':'track'
		});
		_.each( _.times(track.length, row => createTrackCell(trackNum, row)), cell => t.append(cell));
		return t
	}

	const createSongRow = (row) => {
		console.log(song.data.channelCount)
		const rnum = $('<div/>', {
			'class': 'row_number'
		}).html(toHex(row));
		const patterns = _.times(song.data.channelCount, track => $('<div/>',{
			'id':`song_${track}_${row}`,
			'class': 'songCell'
		}).append($('<input/>',{
			'class':'trackInput',
			'value':'--',
			'validator': 'pattern'
		})));
		const songRow = $('<div/>',{
			'id':`songrow_${row}`,
			'class': 'songRow'
		}).append(rnum,patterns);
		songRow.find('input').focus(onCellFocus).keydown(readKey);
		return songRow;
	}



	const createSongEditor = () => {
		return _.times(255, row => createSongRow(row));
	}

	return {
		addMenuItem,
		createTrack,
		setFocusOnCursor,
		createSongEditor
	}
};
